#!/usr/bin/env bash

[[ -f ssh-config ]] || vagrant ssh-config > ssh-config

ansible-playbook -e mode=vagrant setup.yml
